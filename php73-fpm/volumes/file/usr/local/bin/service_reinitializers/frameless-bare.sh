#!/bin/bash

cd /var/www/content/frameless-bare/
install_latest_composer.sh
php -d memory_limit=-1 composer.phar install --no-interaction
